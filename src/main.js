import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { createI18n } from 'vue-i18n'
import { messages } from '../data'

const i18n= createI18n({
    locale:'en',
    messages,
    fallbackWarn: false,
    missingWarn: false
})

createApp(App)
.use(store)
.use(i18n)
// .use(cors)
.use(router)
.use(VueAxios,axios)
.use(ElementPlus)
.mount('#app')
